//
//  ViewController.swift
//  Cap
//
//  Created by satram prudhvi on 2019-11-30.
//  Copyright © 2019 satram prudhvi. All rights reserved.
//

import UIKit
import SpriteKit
import ARKit

class ViewController: UIViewController {

    @IBOutlet var sceneView: ARSKView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let scene =  SKScene(fileNamed: "Scene"){
            sceneView.presentScene(scene)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        //sceneview holds ar session and run the session
        sceneView.session.run(configuration)
        
    }
    
    //to pause we use view will disapppear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
}
extension ViewController:ARSKViewDelegate{
    func session(_ session: ARSession, didFailWithError error: Error) {
        //presents error message
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        //informing about session interupted like presenting an overlay
    }
    func sessionInterruptionEnded(_ session: ARSession) {
            //resets the tracking,removing existing anchors if consistent tracking is required

    }
    
}
